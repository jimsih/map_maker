import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Queue;

public class Planner {
	
	public Planner(){
		
	}
	
	public ArrayList<int[]> planRoute(char[][] frontierMap, int[] robotPosition) {
		return wavefrontPlanner(frontierMap, robotPosition);
	}
	
	public boolean isStuck(char[][] frontierMap, int[] robotPosition) {
		int x = robotPosition[0];
		int y = robotPosition[1];
		
		if (frontierMap[x-1][y] == 'X' && frontierMap[x+1][y] == 'X' &&
			frontierMap[x][y-1] == 'X' && frontierMap[x][y+1] == 'X') {
			return true;
		}
		
		return false;
	}
	
	private ArrayList<int[]> wavefrontPlanner(char[][] frontierMap, int[] robotPosition) {
		/* HashMap: Key: A node, Value: Parent of node */
		HashMap<Node, Node> map = new HashMap<>();
		Queue<Node> queue = new ArrayDeque<>();
		queue.add(new Node(robotPosition));
		
		while (queue.isEmpty() == false) {
			Node node = queue.poll();
			if (frontierMap[node.x][node.y] == 'F') {
				map.remove(new Node(robotPosition));
				return getPath(map, node);
			}
			
			ArrayList<Node> children = getNeighbours(frontierMap, node);
			for (Node child : children) {
				if (map.containsKey(child) == false) {
					map.put(child, node);
					queue.add(child);
				}
			}
		}
		
		return null;
	}
	
	private ArrayList<int[]> getPath(HashMap<Node, Node> map, Node goal) {
		ArrayList<int[]> path = new ArrayList<>();
		Node node = goal;
		path.add(new int[] {node.x, node.y});
		while (map.containsKey(node)) {
			node = map.get(node);
			path.add(new int[] {node.x, node.y});
		}
		Collections.reverse(path);
		return path;
	}
	
	private ArrayList<Node> getNeighbours(char[][] frontierMap, Node pos) {
		
		int maxY = frontierMap[0].length;
		int maxX = frontierMap.length;
		int rightX = pos.x+1; int leftX = pos.x-1;
		int upperY = pos.y+1; int lowerY = pos.y-1;
		
		ArrayList<Node> neighbours = new ArrayList<>();
		
		if (upperY <= maxY) {
			if (frontierMap[pos.x][upperY] == 'O' || frontierMap[pos.x][upperY] == 'F') {
				neighbours.add(new Node(pos.x, upperY));
			}
		}
		
		if (lowerY >= 0) {
			if (frontierMap[pos.x][lowerY] == 'O' || frontierMap[pos.x][lowerY] == 'F') {
				neighbours.add(new Node(pos.x, lowerY));
			}
		}
		
		if (leftX >= 0) {
			if (frontierMap[leftX][pos.y] == 'O' || frontierMap[leftX][pos.y] == 'F') {
				neighbours.add(new Node(leftX, pos.y));
			}
		}
		
		if (rightX <= maxX) {
			if (frontierMap[rightX][pos.y] == 'O' || frontierMap[rightX][pos.y] == 'F') {
				neighbours.add(new Node(rightX, pos.y));
			}
		}
		
		return neighbours;
	}
	
	/* Old, change to map[x][y] */
	private int[] findNearestFrontier(char[][] frontierMap, int[] robotPosition) {
		int maxY = frontierMap.length;
		int maxX = frontierMap[0].length;
		int rightX = robotPosition[0]; int leftX = robotPosition[0];
		int upperY = robotPosition[1]; int lowerY = robotPosition[1];
		
		boolean search = true;
		
		while (search) {
			search = false;
			if (rightX + 1 <= maxX) {
				rightX++;
				search = true;
			}
			if (upperY + 1 <= maxY) {
				upperY++;
				search = true;
			}
			if (leftX - 1 >= 0) {
				leftX--;
				search = true;
			}
			if (lowerY >= 0) {
				lowerY--;
				search = true;
			}
			
			/* Upper row and lower row */
			for (int i=leftX; i<=rightX; i++) {
				if (frontierMap[upperY][i] == 'F') {
					return new int[] {i, upperY};
				}
				
				if (frontierMap[lowerY][i] == 'F') {
					return new int[] {i, lowerY};
				}
			}
			
			/* Left and right column */
			for (int i=lowerY; i<=upperY; i++) {
				if (frontierMap[i][leftX] == 'F') {
					return new int[] {leftX, i};
				}
				
				if (frontierMap[i][rightX] == 'F') {
					return new int[] {rightX, i};
				}
			}
			
		}
		
		return null;		
		
	}
	
	

}
