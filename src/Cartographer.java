import java.util.ArrayList;

public class Cartographer {
	
	/* Possibility of being empty, using column-row indexing */
	public float[][] grid;
	public int x1, y1, x2, y2;
	public int nColumns;
	public int nRows;
	public double sonarMaxRange;
	public double region1Width;
	public double sonarHalfAngle;
	public int cellPerMeter;
	public int cellGrowth;
	
	public Cartographer(int x1, int y1, int x2, int y2) {
		sonarMaxRange = 15.0;
		region1Width = 0.2;
		sonarHalfAngle = Math.toRadians(0.05);
		cellPerMeter = 10;
		cellGrowth = (int) (0.3*cellPerMeter);
		this.x1 = x1*cellPerMeter; this.y1 = y1*cellPerMeter;
		this.x2 = x2*cellPerMeter; this.y2 = y2*cellPerMeter;
		nColumns = this.x2-this.x1;
		nRows = this.y2-this.y1;
		grid = new float[nColumns][nRows];
		for (int i=0; i<nColumns; i++) {
			for (int j=0; j<nRows; j++) {
				grid[i][j] = 0.5f;
			}
		}
		
	}
	
	public void initiateRobotPosition(Position robotPosition) {
		int[] pos = getGridCoordinates(robotPosition);
		for (int i=-2; i<=2; i++) {
			for (int j=-2; j<=2; j++) {
				grid[pos[0]+j][pos[1]+i] = 0;
			}
		}
	}
	
	/**
	 * Updating the grid from sensor data
	 * @param echoes x and y grid-position for each echo
	 * @param laserPosition x and y grid-position for laserposition 
	 */
	public void updateGrid(double[][] echoes, double[] laserPosition) {
		for (int i=0; i<echoes.length; i++) {
			//System.out.println(i);
			sensorModel(echoes[i], laserPosition);
		}
	}
	
	public int[] getGridCoordinates(double[] position) {
		int x = (int) (Math.round(position[0] * cellPerMeter));
		int y = (int) (Math.round(position[1] * cellPerMeter));
		
		int gridX = (x + Math.abs(this.x1));
		int gridY = (Math.abs(this.y1) - y);//(y + Math.abs(this.y1));
		
		return new int[] {gridX, gridY};
	}
	
	public int[] getGridCoordinates(double x, double y) {
		return getGridCoordinates(new double[] {x,y});
	}
	
	public int[] getGridCoordinates(Position pos) {
		return getGridCoordinates(new double[] {pos.getX(), pos.getY()});
	}
	
	public double[] getWorldCoordinates(int gridX, int gridY) {
		double wX = ((gridX-Math.abs(this.x1))/(double)cellPerMeter);
		
		double wY = ((Math.abs(this.y1)-gridY)/(double)cellPerMeter);
		
		return new double[] {wX,wY};
	}
	
	public double[] getWorldCoordinates(int[] pos) {
		return getWorldCoordinates(pos[0], pos[1]);
	}
	
	public ArrayList<Position> getWorldCoordinates(ArrayList<int[]> gridPositions) {
		ArrayList<Position> worldPositions = new ArrayList<>();
		
		for (int[] gPos : gridPositions) {
			worldPositions.add(new Position(getWorldCoordinates(gPos)));
		}
		
		return worldPositions;
	}
	
	/*
	 * Sonar sensor model
	 */
	public void sensorModel(double[] echo, double[] laserPosition) {
		Vector2 direction = new Vector2(echo[0]-laserPosition[0], echo[1]-laserPosition[1]);
		Vector2 ndir = Vector2.normalize(direction);
		double length = Vector2.length(direction);
		
		if (length > sonarMaxRange)
			direction = Vector2.mult(sonarMaxRange, ndir);
		
		double region1Max = length + region1Width;
		double region1Min = length - region1Width;
		Vector2 left = Vector2.rotate(sonarHalfAngle, Vector2.add(direction, Vector2.mult(region1Max, ndir)));
		Vector2 right = Vector2.rotate(-sonarHalfAngle, Vector2.add(direction, Vector2.mult(region1Max, ndir)));
		Vector2 nLeft = Vector2.normalize(left);
		Vector2 nRight = Vector2.normalize(right);
		
		double stepsize = 1.0/cellPerMeter;
		if (Math.abs(nLeft.y) < 0.5 || Math.abs(nRight.y) < 0.5) {
			/* Traverse in x direction */
			
			int dx = 1;
			if (left.x < 0 && right.x < 0)
				dx = -1;
				
			double maxX = Math.max(Math.abs(left.x), Math.abs(right.x));
			int columns = (int) Math.round(maxX * cellPerMeter);
			for (int i=0; Math.abs(i)<=columns; i=i+dx) {
				double x = i*stepsize;
				double leftY = x*(nLeft.y/nLeft.x);
				Vector2 dleft = new Vector2(x, leftY);
				double rightY = x*(nRight.y/nRight.x);
				
				int rows = (int) Math.round(Math.abs(dleft.y-rightY) * cellPerMeter);
				rows = Math.max(rows, 1);
				
				int dy = 1;
				if (left.x < 0 && right.x < 0)
					dy = -1;
				
				for (int j=0; Math.abs(j)<=rows; j=j+dy) {
					Vector2 v = new Vector2(dleft.x, dleft.y+j*stepsize);
					double r = Vector2.length(v);
					double angle = Math.abs(Vector2.angle(direction,v));
					angle = Math.abs(angle-angle/(rows+1));
					
					if (r <= sonarMaxRange && angle <= sonarHalfAngle) {
						double P_S_E = computeSonarProbability(r, region1Max, region1Min, angle);
						int[] gridCoords = getGridCoordinates(new double[] {v.x + laserPosition[0], v.y + laserPosition[1]});
						EvidentialMethod(P_S_E, gridCoords[0], gridCoords[1]);	
					}
				}
			}
		} else {
			/* Traverse in y direction */
			int dy = 1;
			if (left.y < 0 && right.y < 0)
				dy = -1;
			
			double maxY = Math.max(Math.abs(left.y), Math.abs(right.y));
			int rows = (int) Math.round(maxY * cellPerMeter);

			for (int i=0; Math.abs(i)<=rows; i=i+dy) {
				double y = i*stepsize;
				double leftX = y*(nLeft.x/nLeft.y);
				Vector2 dleft = new Vector2(leftX, y);
				double rightX = y*(nRight.x/nRight.y);
				
				int dx = 1;
				if (left.y < 0 && right.y < 0)
					dx = -1;
				
				int columns = (int) Math.round(Math.abs(dleft.x-rightX) * cellPerMeter);
				columns = Math.max(columns, 1);

				for (int j=0; Math.abs(j)<=columns; j=j+dx) {
					Vector2 v = new Vector2(dleft.x+j*stepsize, dleft.y);
					double r = Vector2.length(v);
					double angle = Math.abs(Vector2.angle(direction,v));
					angle = Math.abs(angle - angle/(columns+1));
					
					if (r <= sonarMaxRange && angle <= sonarHalfAngle) {
						int[] gridCoords = getGridCoordinates(new double[] {v.x + laserPosition[0], v.y + laserPosition[1]});

						double P_S_O = computeSonarProbability(r, region1Max, region1Min, angle);
						EvidentialMethod(P_S_O, gridCoords[0], gridCoords[1]);
					}
				}
			}
		}
		
	}
	
	private double computeSonarProbability(double r, double region1Max, double region1Min, double angle) {
		double P_S_O = 0.5;
		if (r < region1Min) {
			/* Region 2 */
			double R = sonarMaxRange;
			double B = sonarHalfAngle;
			double P_S_E = ((R-r)/R + 1)/2;
			P_S_O = 1-P_S_E;
		}
		else if (r < region1Max) {
			/* Region 1 */
			double R = sonarMaxRange;
			double B = sonarHalfAngle;
			P_S_O = 0.98*((R-r)/R + 1) /2;
		}

		return P_S_O;
	}
	
	/*
	 * Using bayesian evidential method, coordinates must be in grid coordinates
	 */
	private void EvidentialMethod(double P_S_O, int gridX, int gridY) {
		if (gridX < 0 || gridX > nColumns-1 || gridY < 0 || gridY > nRows-1)
			return;
		
		grid[gridX][gridY] = (float) ((P_S_O*grid[gridX][gridY])/
				(P_S_O*grid[gridX][gridY] + (1.0-P_S_O)*(1-grid[gridX][gridY])));
		
	}
	
	
	public void printGrid() {
		for (int i=0; i<grid.length; i++) {
			for (int j=0; j<grid[0].length; j++) {
				System.out.format("%.2f ", grid[i][j]);
			}	
			System.out.println("");
		}
	}
	
	public void printFrontierMap(char[][] frontierMap, int[] robotPosition, ArrayList<int[]> path) {
		int[] goal = path.get(path.size()-1);
		frontierMap[goal[0]][goal[1]] = 'G';
		
		for (int i=0; i<path.size()-1; i++) {
			int[] p = path.get(i);
			frontierMap[p[0]][p[1]] = 'P';
		}
		frontierMap[robotPosition[0]][robotPosition[1]] = 'R';
		printFrontierMap(frontierMap);
	}
	
	public void printFrontierMap(char[][] frontierMap, int[] robotPosition, int[] goal) {
		frontierMap[robotPosition[0]][robotPosition[1]] = 'R';
		frontierMap[goal[0]][goal[1]] = 'G';
		printFrontierMap(frontierMap);
	}
	
	public void printFrontierMap(char[][] frontierMap, int[] robotPosition) {
		frontierMap[robotPosition[0]][robotPosition[1]] = 'R';
		printFrontierMap(frontierMap);
	}
	
	public void printFrontierMap(char[][] frontierMap) {
		for (int i=0; i<grid[0].length; i++) {
			for (int j=0; j<grid.length; j++) {
				System.out.print(frontierMap[j][i]);
			}
			System.out.println();
		}
	}
	
	public char[][] getFrontierMap() {
		char[][] frontierMap = new char[grid.length][grid[0].length];
		for (int i=1; i<grid[0].length-1; i++) {
			for (int j=1; j<grid.length-1; j++) {
				if (grid[j][i] > 0.5) {
					frontierMap[j][i] = 'X';
				} else if (grid[j][i] < 0.3) {
					frontierMap[j][i] = 'O';
				} else {
					frontierMap[j][i] = 'U';
				}	
			}	
		}
		frontierMap = obsticleGrowing(frontierMap);
		frontierMap = addFrontiers(frontierMap);
		return frontierMap;
	}
	
	private char[][] addFrontiers(char[][] Cspace) {
		for (int i=1; i<Cspace[0].length-1; i++) {
			for (int j=1; j<Cspace.length-1; j++) {
				if (Cspace[j][i] == 'O') {
					if (isFrontier(j, i, Cspace)) {
						Cspace[j][i] = 'F';
					}
				}
			}	
		}
		
		return Cspace;
	}
	
	private char[][] obsticleGrowing(char[][] map) {
		ArrayList<int[]> obsticles = new ArrayList<>();
		for (int i=0; i<map[0].length; i++) {
			for (int j=0; j<map.length; j++) {
				if (map[j][i] == 'X') {
					obsticles.add(new int[] {j,i});
				}
			}	
		}
		
		for (int[] obsticle : obsticles) {
			for (int i=-cellGrowth; i<=cellGrowth; i++) {
				for (int j=-cellGrowth; j<=cellGrowth; j++) {
					if (obsticle[0]+j < 0 || obsticle[0]+j >= map.length ||
						obsticle[1]+i < 0 || obsticle[1]+i >= map[0].length) {
						continue;
					}
					map[obsticle[0]+j][obsticle[1]+i] = 'X';
				}
			}
		}
		
		return map;
	}
	
	private boolean isFrontier(int column, int row, char[][] Cspace) {
		if (Cspace[column+1][row] == 'U' ||
			Cspace[column][row-1] == 'U' || Cspace[column][row+1] == 'U' ||
			Cspace[column-1][row] == 'U') {
			return true;
		}
		return false;
	}
}
