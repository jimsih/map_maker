import java.util.Objects;

public class Node {

	public int x, y;
	
	public Node(int[] pos) {
		this.x = pos[0];
		this.y = pos[1];
	}
	
	public Node(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	@Override
    public int hashCode() {
       return Objects.hash(this.x, this.y);
	}
	
	@Override
    public boolean equals(Object obj) {
		if (obj == this)
            return true;
		if (obj instanceof Node) {
			Node node = (Node)obj;
			if (this.x == node.x && this.y == node.y) {
				return true;
			}
		}
        
        return false;
    }
}
