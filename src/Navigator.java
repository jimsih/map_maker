import java.util.ArrayList;

public class Navigator {
	
	public Navigator() {
		
	}
	
	public void navigate(ArrayList<Position> path, Controller controller) {
		for(int i  = 0 ; i < path.size() ; i++) {
			Position nextPos = path.get(i);
			Position pos = controller.getRobotPosition();
				
		    double distance = Math.sqrt(Math.pow((nextPos.getY()-pos.getY()),2) + Math.pow((nextPos.getX()-pos.getX()),2));
			
			if(distance < 0.5) {
				continue;
			}	
			
			try {
				if (!controller.run(nextPos)) {
					return;
				}
				if (controller.getRobotPosition().getDistanceTo(nextPos) > 0.6) {
					return;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
