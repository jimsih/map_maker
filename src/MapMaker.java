import java.util.ArrayList;

public class MapMaker {
	
	public static boolean debug = false;
	
	public MapMaker() {
		
	}
	
	private void validateInput(String[] args) {
		if (args.length != 5) {
			System.out.println("Usage: MapMaker url x1 y1 x2 y2");
			System.exit(-1);
		}
		
		try {
			int x1 = Integer.parseInt(args[1]);
			int y1 = Integer.parseInt(args[2]);
			int x2 = Integer.parseInt(args[3]);
			int y2 = Integer.parseInt(args[4]);
		} catch(NumberFormatException e) {
			System.out.println("Coordinates must be integers");
			System.exit(-1);
		}
		
		try {
			String adress = args[0].substring(0, args[0].lastIndexOf(':'));
			int port = Integer.parseInt(args[0].substring(args[0].lastIndexOf(':')+1));
		} catch (Exception e) {
			System.out.println("URL is invalid");
			System.exit(-1);
		}

	}

	public static void main(String[] args) throws Exception {
		
		MapMaker mapMaker = new MapMaker();
		mapMaker.validateInput(args);
		
		String adress = args[0].substring(0, args[0].lastIndexOf(':'));
		int port = Integer.parseInt(args[0].substring(args[0].lastIndexOf(':')+1));
		int x1 = Integer.parseInt(args[1]);
		int y1 = Integer.parseInt(args[2]);
		int x2 = Integer.parseInt(args[3]);
		int y2 = Integer.parseInt(args[4]);
		
		Cartographer cg = new Cartographer(x1, y1, x2, y2);
		ShowMap sm = new ShowMap(cg.nRows, cg.nColumns, true);
		Controller cr = new Controller(adress, port, cg, sm);
		cg.initiateRobotPosition(cr.getRobotPosition());
		Navigator navigator = new Navigator();
		Planner planner = new Planner();
		
		cr.scanner(true);
		cr.rotate(2*Math.PI);
		
		int[] p = cr.cg.getGridCoordinates(cr.getRobotPosition());
		ArrayList<int[]> path = planner.planRoute(cr.cg.getFrontierMap(), p);
		
		int robotStillCounter = 0;
		int[] lastGridCoordinates = p;
		while (path != null) {
			if (path.size() == 1 || path.size() == 2) {
				cr.rotate(2*Math.PI);
			}

			navigator.navigate(cr.cg.getWorldCoordinates(path), cr);
			
			p = cr.cg.getGridCoordinates(cr.getRobotPosition());
			char[][] frontierMap = cr.cg.getFrontierMap();
			path = planner.planRoute(frontierMap, p);
			
			if (path == null) {
				if (planner.isStuck(frontierMap, p)) {
					do {
						if (debug) {
							System.out.println("stuck");
						}
						
						cr.moveFromObstacle();
						p = cr.cg.getGridCoordinates(cr.getRobotPosition());
						path = planner.planRoute(cr.cg.getFrontierMap(), p);
					} while (planner.isStuck(frontierMap, p));
					
				}
			}
			
			if (lastGridCoordinates[0] == p[0] && lastGridCoordinates[1] == p[1]) {
				robotStillCounter++;
				if (robotStillCounter >= 5) {
					cr.moveFromObstacle();
				}
				
			} else {
				lastGridCoordinates[0] = p[0];
				lastGridCoordinates[1] = p[1];
				robotStillCounter = 0;
			}
		}
//		p = cr.cg.getGridCoordinates(cr.getRobotPosition());
		if (debug) {
			cr.cg.printFrontierMap(cr.cg.getFrontierMap(), p);
		}
		

		System.out.println("Finnished");
	}

}
