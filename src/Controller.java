
public class Controller {
	LocalizationResponse lr;
	LaserEchoesResponse ler;
	LaserPropertiesResponse lpr;
	DifferentialDriveRequest dr;
	RobotCommunication rc;
	
	private static double speed = 0.5;
	private static double angularSpeed = 0.25; /* Radians per second */
	private static double robotSize = 0.2;
	private static int obstacleScanWidth = 25;
	private static int sonarHeadAngle = 135;
	private Scanner scanner;
	
	private double[] laserPosition;
	private double[] robotPosition;
	private double[] laserAngles;
	
	public Cartographer cg;
	public ShowMap map;
	
	public Controller(String host, int port, Cartographer cg, ShowMap map) throws Exception {
		lr = new LocalizationResponse();
		ler = new LaserEchoesResponse();
		lpr = new LaserPropertiesResponse();
		dr = new DifferentialDriveRequest();
		rc = new RobotCommunication(host, port);
		this.cg = cg;
		scanner = null;
		this.map = map;
		
		rc.getResponse(lpr);
		laserAngles = getLaserAngles(lpr);
	}
	public Controller() {
		
	}
	
	/* Returns if robot is able to move to next position */
	public boolean run(Position nextPos) throws Exception {
		
		rc.getResponse(lr);
		Position pos = getRobotPosition();
			
	    double dist = pos.getDistanceTo(nextPos);
		double bearing = pos.getBearingTo(nextPos);
		double heading = lr.getHeadingAngle();
		
		double angle = getAngle(heading, bearing);
		if (obsticle(dist, angle)) {
			if (MapMaker.debug) {
				System.out.println("obsticle");
			}
			return false;
		}
		
		/* Rotate robot */
		rotate(heading, bearing, dr);		
		
		drive(dist);
	    
	    return true;
	}
	
	public void drive(double distance) {
		try {
			if (distance > 0) {
				dr.setLinearSpeed(speed);
			} else {
				dr.setLinearSpeed(-speed);
			}
			
			distance = Math.abs(distance);

			rc.putRequest(dr);
			Thread.sleep((long)(distance*1000/speed));
			dr.setLinearSpeed(0);
			rc.putRequest(dr);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void moveBack(double distance) {
		drive(-distance);
	}
	
	private boolean obsticle(double distance, double angle) {
		double degrees = Math.toDegrees(-angle);
		int scanAngle = (int) (135+degrees);
		if (scanAngle < 0 || scanAngle > 270) {
			return false;
		}
		double[] echoes = ler.getEchoes();
		for (int i=Math.max(scanAngle-obstacleScanWidth, 0); i<=Math.min(scanAngle+obstacleScanWidth, 270); i++) {
			if (echoes[i] < distance + robotSize) {
				return true;
			}
		}
		return false;
	}
	
	public void moveFromObstacle() {
		if (MapMaker.debug) {
			System.out.println("Moving from obstacle");
		}
		
		double obstacleDistance = 0.4;
		double[] echoes = ler.getEchoes();
		double weightedObstacleDegree = 0;
		int counter = 0;
		for (int i=0; i<echoes.length; i++) {
			if (echoes[i] < robotSize+obstacleDistance) {
				weightedObstacleDegree += i;
				counter++;
			}
		}
		
		weightedObstacleDegree = weightedObstacleDegree / counter;
		
		double rotate = 0;
		if (weightedObstacleDegree < 135) {
			rotate = Math.toRadians((weightedObstacleDegree - sonarHeadAngle) + 180);
		} else {
			rotate = Math.toRadians((weightedObstacleDegree - sonarHeadAngle) - 180);
		}

		rotate(rotate);
		drive(2*robotSize);
	}
	
	/* Drives forward if possible */
	private boolean moveForward(double distance) {
		boolean obstacle = false;
		double[] echoes = ler.getEchoes();
		for (int i=0; i<=obstacleScanWidth; i++) {
			if (echoes[135+i] < distance + robotSize) {
				obstacle = true;
				break;
			}
			if (echoes[135-i] < distance + robotSize) {
				obstacle = true;
				break;
			}
		}
		
		if (obstacle) {
			return false;
		} else {
			drive(distance);
			return true;
		}
	}
	
	public void rotate(double angle) {
		try {
			if (angle > 0) {
				dr.setAngularSpeed(Math.PI * angularSpeed);
			} else {
				dr.setAngularSpeed(-Math.PI * angularSpeed);
			}
			
			angle = Math.abs(angle);
			
			rc.putRequest(dr);
			Thread.sleep((long) (Math.abs((angle/(Math.PI*angularSpeed))*1000)));
			dr.setAngularSpeed(0);
			rc.putRequest(dr);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("No connection");
		}
	}
	
	private double getAngle(double heading, double bearing) {
		if(bearing < 0 ) {
			bearing += 2*Math.PI;
		}
		if(heading < 0) {
			heading += 2*Math.PI;
		}
		
		double diff = heading - bearing;
		
		if(diff > Math.PI) {
			diff -= 2*Math.PI;
		} else if(diff < -Math.PI) {
			
			diff += 2*Math.PI;
		}
		
		return diff;
	}
	
	private void rotate(double heading, double bearing, DifferentialDriveRequest dr) throws Exception {
		double diff = getAngle(heading, bearing);
		
		if (diff > 0) {
		dr.setAngularSpeed(-Math.PI * angularSpeed);
		} else if (diff < 0) {
		dr.setAngularSpeed(Math.PI * angularSpeed);
		}
	
		rc.putRequest(dr);
		Thread.sleep((long) (Math.abs((diff/(Math.PI*angularSpeed))*1000)));
		dr.setAngularSpeed(0);
		rc.putRequest(dr);
	}
	
	public double[][] scanEnvironment() {
		try {
			rc.getResponse(ler);
			rc.getResponse(lpr);
			rc.getResponse(lr);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("No connection");
		}
		double[] echoes = ler.getEchoes();
		double[] robotLaserPosition = lpr.getPosition();
		double heading = lr.getHeadingAngle();
		robotPosition = lr.getPosition();
		
		double laserDistance = Math.sqrt(Math.pow(robotLaserPosition[0], 2) + Math.pow(robotLaserPosition[1], 2));
		laserPosition = new double[] {
							robotPosition[0] + Math.cos(heading)*laserDistance,
							robotPosition[1] + Math.sin(heading)*laserDistance
							};
		
		double[][] obsticlePositions = new double[echoes.length][2];
		for (int i=0; i<echoes.length; i++) {
			double obsticleAngle = heading + laserAngles[i];
			double posX = Math.cos(obsticleAngle)*echoes[i] + laserPosition[0];
			double posY = Math.sin(obsticleAngle)*echoes[i] + laserPosition[1];
			obsticlePositions[i][0] = posX;
			obsticlePositions[i][1] = posY;
		}
		
		return obsticlePositions;
	}
	
	/**
	 * Get corresponding angles to each laser beam
	 * 
	 * @param lpr
	 * @return laser angles in radians
	 */
	public double[] getLaserAngles(LaserPropertiesResponse lpr) {
		int beamCount = (int) ((lpr.getEndAngle() - lpr.getStartAngle()) / lpr
				.getAngleIncrement()) + 1;
		double[] angles = new double[beamCount];
		double a = lpr.getStartAngle();
		for (int i = 0; i < beamCount; i++) {
			angles[i] = a;
			// We get roundoff errors if we use AngleIncrement. Use 1 degree in
			// radians instead
			a += 1 * Math.PI / 180;// lpr.getAngleIncrement();
		}
		return angles;
	}
	
	public Position getRobotPosition() {
		try {
			rc.getResponse(lr);
			double[] pos = lr.getPosition();
			return new Position(pos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void scanner(boolean v) {
		if (v == true) {
			if (scanner == null) {
				scanner = new Scanner();
				scanner.run = true;
				scanner.start();
			}
		} else {
			if (scanner != null) {
				scanner.run = false;
				try {
					scanner.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				scanner = null;
			}
		}
	}

	class Scanner extends Thread {
		public boolean run = true;
		
		private static final long scanFrequence = 1;

		@Override
		public void run() {
			while (run) {
				double[][] echoes = scanEnvironment();
				cg.updateGrid(echoes, laserPosition);
				int[] robPos = cg.getGridCoordinates(robotPosition);
				map.updateMap(cg.grid, robPos[0], robPos[1]);
				try {
					Thread.sleep(scanFrequence*100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}
		
	}
}
