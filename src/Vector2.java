
public class Vector2 {
	
	public double x;
	public double y;
	
	public Vector2(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	static public Vector2 add(Vector2 v1, Vector2 v2) {
		return new Vector2(v1.x + v2.x, v1.y + v2.y);
	}
	
	static public Vector2 mult(double a, Vector2 v) {
		return new Vector2(a*v.x, a*v.y);
	}
	
	static public double angle(Vector2 v1, Vector2 v2) {
		return Math.acos(Vector2.dot(Vector2.normalize(v1), Vector2.normalize(v2)));
	}
	
	static public double dot(Vector2 v1, Vector2 v2) {
		return (v1.x*v2.x + v1.y*v2.y);
	}
	
	static public Vector2 rotate(double rad, Vector2 v) {
		double newX = v.x*Math.cos(rad) - v.y*Math.sin(rad); 
		double newY = v.x*Math.sin(rad) + v.y*Math.cos(rad);
		return new Vector2(newX, newY);
	}
	
	static public Vector2 normalize(Vector2 v) {
		double length = Vector2.length(v);
		Vector2 vec = new Vector2(v.x/length, v.y/length);
		return vec;
	}
	
	static public Vector2 normalize(double x, double y) {
		double length = Vector2.length(x, y);
		Vector2 vec = new Vector2(x/length, y/length);
		return vec;
	}
	
	static double length(Vector2 v) {
		return Math.sqrt(Math.pow(v.x, 2) + Math.pow(v.y, 2));
	}
	
	static double length(double x, double y) {
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}
	
	static double toRadians(double degree) {
		return degree*Math.PI/180;
	}

}
